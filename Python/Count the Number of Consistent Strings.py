"""
You are given a string allowed consisting of distinct characters and an array of strings words. A string is consistent if all characters in the string appear in the string allowed.

Return the number of consistent strings in the array words.

 

Example 1:

Input: allowed = "ab", words = ["ad","bd","aaab","baa","badab"]
Output: 2
Explanation: Strings "aaab" and "baa" are consistent since they only contain characters 'a' and 'b'.
Example 2:

Input: allowed = "abc", words = ["a","b","c","ab","ac","bc","abc"]
Output: 7
Explanation: All strings are consistent.
Example 3:

Input: allowed = "cad", words = ["cc","acd","b","ba","bac","bad","ac","d"]
Output: 4
Explanation: Strings "cc", "acd", "ac", and "d" are consistent.
 

Constraints:

1 <= words.length <= 104
1 <= allowed.length <= 26
1 <= words[i].length <= 10
The characters in allowed are distinct.
words[i] and allowed contain only lowercase English letters.
"""

# Code
class Solution:
    def countConsistentStrings(self, allowed: str, words: List[str]) -> int:
        allowed_arr = []
        result = 0
        for _ in range(len(allowed)):
            allowed_arr.append(allowed[_])
            
        for j in words:
            for k in range(len(j)):
                if j[k] not in allowed_arr:
                    break
                if (j[k] in allowed_arr) and k == (len(j)-1):
                    result += 1
        return(result)


"""
74 / 74 test cases passed.
Status: Accepted
Runtime: 344 ms
Memory Usage: 16.4 MB
"""
