"""
A self-dividing number is a number that is divisible by every digit it contains.

For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.

Also, a self-dividing number is not allowed to contain the digit zero.

Given a lower and upper number bound, output a list of every possible self dividing number, including the bounds if possible.

Example 1:
Input: 
left = 1, right = 22
Output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]
Note:

The boundaries of each input argument are 1 <= left <= right <= 10000.
"""

# Code
class Solution:
    def selfDividingNumbers(self, left: int, right: int) -> List[int]:
        output = []
        for _ in range(left,right+1):
            count = 0
            # print(_)
            list_int = [int(d) for d in str(_)]
            for idx in list_int:
                try:
                    if _ % idx == 0:
                        count += 1
                    else:
                        break
                except ZeroDivisionError:
                    break
            if count == len(list_int):
                output.append(_)
                # print("Output appended ", output)
        return(output)


"""
31 / 31 test cases passed.
Status: Accepted
Runtime: 64 ms
Memory Usage: 14.3 MB
"""
