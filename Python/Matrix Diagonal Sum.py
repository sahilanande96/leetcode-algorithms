"""
Given a square matrix mat, return the sum of the matrix diagonals.

Only include the sum of all the elements on the primary diagonal and all the elements on the secondary diagonal that are not part of the primary diagonal.

 

Example 1:


Input: mat = [[1,2,3],
              [4,5,6],
              [7,8,9]]
Output: 25
Explanation: Diagonals sum: 1 + 5 + 9 + 3 + 7 = 25
Notice that element mat[1][1] = 5 is counted only once.
Example 2:

Input: mat = [[1,1,1,1],
              [1,1,1,1],
              [1,1,1,1],
              [1,1,1,1]]
Output: 8
Example 3:

Input: mat = [[5]]
Output: 5
 

Constraints:

n == mat.length == mat[i].length
1 <= n <= 100
1 <= mat[i][j] <= 100
"""

# Code
class Solution:
    def diagonalSum(self, mat: List[List[int]]) -> int:
        # for even nos
        prim_dia = 0
        sec_dia = 0
        result = 0
        # if len(mat) % 2 == 0:
        #     for _ in range(len(mat)):
        #         prim_dia = mat[_][_] + prim_dia
        #         sec_dia = mat[_][len(mat)-1 - _] + sec_dia
        #     result = prim_dia + sec_dia
        #     return(result)
        # else:
        #     for _ in range(len(mat)):
        #         prim_dia = mat[_][_] + prim_dia
        #         sec_dia = mat[_][len(mat)-1 - _] + sec_dia
        #     result = prim_dia + sec_dia -mat[len(mat)//2][len(mat)//2]
        #     return(result)
        
        for _ in range(len(mat)):
            prim_dia = mat[_][_] + prim_dia
            sec_dia = mat[_][len(mat)-1 - _] + sec_dia
            
        if len(mat) % 2 != 0:
            result = prim_dia + sec_dia -mat[len(mat)//2][len(mat)//2]
            return(result)
        else:
            result = prim_dia + sec_dia
            return(result)


"""
113 / 113 test cases passed.
Status: Accepted
Runtime: 104 ms
Memory Usage: 14.4 MB
"""
