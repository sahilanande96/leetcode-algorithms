"""
You are given an integer array nums. The unique elements of an array are the elements that appear exactly once in the array.

Return the sum of all the unique elements of nums.

 

Example 1:

Input: nums = [1,2,3,2]
Output: 4
Explanation: The unique elements are [1,3], and the sum is 4.
Example 2:

Input: nums = [1,1,1,1,1]
Output: 0
Explanation: There are no unique elements, and the sum is 0.
Example 3:

Input: nums = [1,2,3,4,5]
Output: 15
Explanation: The unique elements are [1,2,3,4,5], and the sum is 15.
 

Constraints:

1 <= nums.length <= 100
1 <= nums[i] <= 100
"""

# Code
class Solution:
    def sumOfUnique(self, nums: List[int]) -> int:
        no_dub, dub_keys = {}, set()
        
        for _ in nums:
            if _ not in no_dub:
                no_dub[_] = "new"
            else:
                dub_keys.add(_)
        # print(no_dub)
        # print(list(dub_keys))
        for idx in list(dub_keys):
            del no_dub[idx]
        # print(no_dub)
        return(sum(x for x in no_dub.keys()))


"""
73 / 73 test cases passed.
Status: Accepted
Runtime: 28 ms
Memory Usage: 14.4 MB
"""
