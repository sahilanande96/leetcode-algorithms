"""
Given an array of integers nums, sort the array in ascending order.

 

Example 1:

Input: nums = [5,2,3,1]
Output: [1,2,3,5]
Example 2:

Input: nums = [5,1,1,2,0,0]
Output: [0,0,1,1,2,5]
 

Constraints:

1 <= nums.length <= 5 * 104
-5 * 104 <= nums[i] <= 5 * 104
"""

# Code
class Solution: 
    def mergeSort(self, nums):
        if len(nums)==1:
            return nums
        mid = (len(nums)-1) // 2
        lst1 = self.mergeSort(nums[:mid+1])
        lst2 = self.mergeSort(nums[mid+1:])
        result = self.merge(lst1, lst2)
        return result
    
    def merge(self, lst1, lst2):
        lst = []
        i = 0
        j = 0
        while(i<=len(lst1)-1 and j<=len(lst2)-1):
            if lst1[i]<lst2[j]:
                lst.append(lst1[i])
                i+=1
            else:
                lst.append(lst2[j])
                j+=1
        if i>len(lst1)-1:
            while(j<=len(lst2)-1):
                lst.append(lst2[j])
                j+=1
        else:
            while(i<=len(lst1)-1):
                lst.append(lst1[i])
                i+=1
        return lst
    
    def sortArray(self, nums: List[int]) -> List[int]:
        sorted_list = self.mergeSort(nums)
        return sorted_list

"""
13 / 13 test cases passed.
Status: Accepted
Runtime: 1797 ms
Memory Usage: 22.9 MB
"""
