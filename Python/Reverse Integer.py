"""
Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.

Assume the environment does not allow you to store 64-bit integers (signed or unsigned).

 

Example 1:

Input: x = 123
Output: 321
Example 2:

Input: x = -123
Output: -321
Example 3:

Input: x = 120
Output: 21
Example 4:

Input: x = 0
Output: 0
 

Constraints:

-231 <= x <= 231 - 1
"""

#Code
class Solution:
    def reverse(self, x: int) -> int:
        str_abs = str(abs(x))
        if x < 0:
            result = -1 * int(str_abs[::-1])
        else:
            result = int(str_abs[::-1])
            
        if (((-2)**31) <= result <= ((2**31) - 1) ):
            return(result)
        else:
            return(0)
            
        

"""
1032 / 1032 test cases passed.
Status: Accepted
Runtime: 32 ms
Memory Usage: 14.3 MB
"""
